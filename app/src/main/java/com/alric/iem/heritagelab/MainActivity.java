package com.alric.iem.heritagelab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.alric.iem.heritagelab.vehicles.Car;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
