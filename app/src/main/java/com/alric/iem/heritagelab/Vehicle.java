package com.alric.iem.heritagelab;

/**
 * Created by iem on 19/10/2016.
 */

public abstract class Vehicle
{
    private int nbWheels;
    private int weight; //En kg
    private String model;
    private int price;
    private String commissionning;


    public int getNbWheels() {
        return nbWheels;
    }

    public void setNbWheels(int nbWheels) {
        this.nbWheels = nbWheels;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCommissionning() {
        return commissionning;
    }

    public void setCommissionning(String commissionning) {
        this.commissionning = commissionning;
    }

}
