package com.alric.iem.heritagelab;

import android.media.Image;

import java.util.ArrayList;

/**
 * Created by iem on 19/10/2016.
 */

public class Afiliate {

    protected String name;
    protected Image logo;
    protected Long revenue;
    protected String origin;
    protected int nbStaff;
    protected String buyingDate;

    public ArrayList<Vehicle> getListVehicle() {
        return listVehicle;
    }

    public void setListVehicle(ArrayList<Vehicle> listVehicle) {
        this.listVehicle = listVehicle;
    }

    protected ArrayList<Vehicle> listVehicle;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getLogo() {
        return logo;
    }

    public void setLogo(Image logo) {
        this.logo = logo;
    }

    public Long getRevenue() {
        return revenue;
    }

    public void setRevenue(Long revenue) {
        this.revenue = revenue;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getNbStaff() {
        return nbStaff;
    }

    public void setNbStaff(int nbStaff) {
        this.nbStaff = nbStaff;
    }

    public String getBuyingDate() {
        return buyingDate;
    }

    public void setBuyingDate(String buyingDate) {
        this.buyingDate = buyingDate;
    }

}
