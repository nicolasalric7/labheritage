package com.alric.iem.heritagelab.vehicles;

import com.alric.iem.heritagelab.Vehicle;

/**
 * Created by iem on 19/10/2016.
 */

public class Motorbike extends Vehicle
{
    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    int cylinder;
}
