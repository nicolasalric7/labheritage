package com.alric.iem.heritagelab.vehicles;

import com.alric.iem.heritagelab.Vehicle;

/**
 * Created by iem on 19/10/2016.
 */

public class Car extends Vehicle {
    public int getNbSeats() {
        return nbSeats;
    }

    public void setNbSeats(int nbSeats) {
        this.nbSeats = nbSeats;
    }

    protected int nbSeats;

}
